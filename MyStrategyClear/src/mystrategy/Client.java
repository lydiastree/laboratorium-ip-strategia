/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mystrategy;

import data.DataGenerator;
import sort.methods.Bubblesort;

/**
 *
 * @author LeopardProMK
 */
public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /* Algorytmy sortowania */
        // http://www.algorytm.org/algorytmy-sortowania/
        
        double[] dataNonSort = DataGenerator.generate(100000);
        
		Context con = new Context(new Bubblesort());
		long start = System.currentTimeMillis();
		con.perform(dataNonSort);
		long stop = System.currentTimeMillis();
		System.out.println("Bubblesort: " + elapsedTime + " ms");
			
		con = new Context(new Selectionsort());
		long start = System.currentTimeMillis();
		con.perform(dataNonSort);
		long stop = System.currentTimeMillis();
		System.out.println("Selectionsort: " + elapsedTime + " ms");
		
		con = new Context(new Insertionsort());
		long start = System.currentTimeMillis();
		con.perform(dataNonSort);
		long stop = System.currentTimeMillis();
		System.out.println("Insertionsort: " + elapsedTime + " ms");
		
		con = new Context(new Quicksort());
		long start = System.currentTimeMillis();
		con.perform(dataNonSort);
		long stop = System.currentTimeMillis();
		System.out.println("Quicksort: " + elapsedTime + " ms");
		
        //Bubblesort ob = new Bubblesort();
        //long start = System.currentTimeMillis();
        //ob.Sort(dataNonSort);
        //long stop = System.currentTimeMillis();
       /* Wzorzec Stratega */
        //...
       
        System.out.println("Time: " + (stop - start) + "ms");
    }
}
