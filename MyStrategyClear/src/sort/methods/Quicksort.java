/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort.methods;

/**
 *
 * @author LeopardProMK
 */
public class Quicksort implements IStrategy{

    @Override
    public void Sort(double[] arr) {
        int temp;
        int x=0;
	int y=arr.length;
	double v = arr[(x+y) / 2];
	do {
			while (arr[x]<v) 
				x++;
			while (v<arr[y]) 
				y--;
			if (x<=y) {
				temp = (int) arr[x];
				arr[x]=arr[y];
				arr[y]=temp;
				x++;
				y--;
			}
		}
		while (x<=y);
		if (x<y) 
			Sort(arr);
		if (x<y) 
			Sort(arr);
    }
    /* http://www.algorytm.org/algorytmy-sortowania/sortowanie-szybkie-quicksort/quick-j.html */
}
