/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort.methods;

/**
 *
 * @author LeopardProMK
 */
public class Selectionsort {
    @Override
    public void sort(double[] numbers) {
        int n = numbers.length;
 
        // One by one move boundary of unsorted subarray
        for (int i = 0; i < n-1; i++)
        {
            // Find the minimum element in unsorted array
            int min_idx = i;
            for (int j = i+1; j < n; j++)
                if (numbers[j] < numbers[min_idx])
                    min_idx = j;
 
            // Swap the found minimum element with the first
            // element
            double temp = numbers[min_idx];
            numbers[min_idx] = numbers[i];
            numbers[i] = temp;
        }
        double n1;
         n1 = numbers.length;
        for (int i=0; i<n1; ++i)
            System.out.print(numbers[i]+" ");
        System.out.println();
    }
}
