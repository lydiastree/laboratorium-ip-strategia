/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort.methods;

import java.util.Arrays;
import java.util.Collections;
import java.util.OptionalDouble;

/**
 *
 * @author LeopardProMK
 */
public class Bubblesort implements IStrategy {
    /* http://www.algorytm.org/algorytmy-sortowania/sortowanie-babelkowe-bubblesort.html */
    @Override
    public void Sort(double arr[])
    {
        int n = arr.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (arr[j] > arr[j+1])
                {
                    double temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
    }
 
    void printArray(double arr[])
    {
        Sort(arr);
        OptionalDouble max = Arrays.stream(arr).max();
        OptionalDouble min = Arrays.stream(arr).min();
        System.out.println( "min " + min + ", max " + max);
    }
}
